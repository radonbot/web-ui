from flask import Flask, Response, url_for, render_template, redirect, request, session, abort
from bson import json_util, ObjectId
import json
import pymongo
import requests
import secrets
import os
import time
import mimetypes
import sys

sessions = {}

API_ENDPOINT = "https://discord.com/api/v8"

try:
    FLASK_SECRET_KEY = os.environ["FLASK_SECRET_KEY"]
    DISCORD_CLIENT_ID = os.environ["DISCORD_CLIENT_ID"]
    DISCORD_CLIENT_SECRET = os.environ["DISCORD_CLIENT_SECRET"]
    REDIRECT_URI = os.environ["REDIRECT_URI"]
    DISCORD_BOT_TOKEN = os.environ["DISCORD_BOT_TOKEN"]

except NameError as e:
    print(f"Missing environment variable {e}")
    sys.exit()


with open("/config/config.json", "r") as f:
    CFG = json.load(f)


app = Flask(__name__)
app.secret_key = FLASK_SECRET_KEY
db_client = pymongo.MongoClient(CFG["connectionString"])["discord_bot"]
sounds = db_client["sounds"]
users = db_client["users"]
guilds = db_client["guilds"]
logs = db_client["logging"]


@app.route("/api/v1/guilds")
def get_guilds():
    token = session.get("TOKEN")
    return Response(
        json_util.dumps(
            {"guilds": sessions[token]["guilds"].values()}
        )
    )


def get_user_guilds(access_token):
    header = {"Authorization": f"Bearer {access_token}"}
    guilds = requests.get(
        f"{API_ENDPOINT}/users/@me/guilds", headers=header).json()
    return guilds


@app.route("/api/v1/commands/<guild_id>")
def get_commands(guild_id):
    return Response(
        json_util.dumps({"commands": list(
            sounds.find(
                {"guild_id": ObjectId(guild_id)}
                ).sort("name", pymongo.ASCENDING))}),
        mimetype="application/json")


@app.route("/api/v1/logs/<guild_id>/<_type>/<limit>")
def get_logs(guild_id, _type, limit):
    return Response(
        json_util.dumps({"logs": list(
            logs.find(
                {"guild_id": ObjectId(guild_id),
                 "type": str(_type)},
                {"messages": 0,
                 "guild_id": 0}).sort("timestamp", pymongo.DESCENDING)
                                .limit(int(limit)))[::-1]}),
        mimetype="application/json")


@app.route("/api/v1/logchain/<chain_id>")
def get_chain(chain_id):
    return Response(
        json_util.dumps({"logs": list(
            logs.find(
                {"_id": ObjectId(chain_id)}
                ))}),
        mimetype="application/json")


@app.route("/api/v1/user/<user_id>")
def get_user(user_id):
    return Response(
        json_util.dumps({"user": users.find_one({"_id": ObjectId(user_id)})}),
        mimetype="application/json"
    )


@app.route("/list/<guild_id>")
def command_list(guild_id):
    return render_template("/index.html", guild_id=guild_id)


@app.route("/fb")
def filter_builder():
    return render_template("/fb.html")


@app.route("/guilds")
def guilds_view():
    return render_template("/guilds.html")


@app.route("/api/v1/resource/<image_repo>/<image_type>/<discord_id>")
def resource(image_type, discord_id, image_repo):
    discord_id = discord_id.replace(".", "").replace("/", "")
    image_type = image_type.replace(".", "").replace("/", "")
    image_repo = image_repo.replace(".", "").replace("/", "")

    if(image_type == "webp"):
        mime = "image/webp"
    else:
        mime = "image/gif"

    if image_repo == "profilePic":
        with open(f"/profilePics/{discord_id}.{image_type}", "rb") as f:
            return Response(f.read(), mimetype=mime)
    elif image_repo == "guildPic":
        with open(f"/guildPics/{discord_id}.{image_type}", "rb") as f:
            return Response(f.read(), mimetype=mime)


@app.route("/api/v1/files/sounds/<sound_id>")
def download_sound(sound_id):
    db_obj = sounds.find_one({
        "_id": ObjectId(sound_id)
    })
    if db_obj is None:
        db_obj = sounds.find_one({
            "_id": str(sound_id)
        })
    if db_obj is not None:
        guild_id = db_obj["guild_id"]
        if("cached_name" in db_obj):
            path = f"/bot_files/{guild_id}/trimmed_clips/{db_obj['cached_name']}"
        elif ("type" in db_obj and db_obj['type'] == "yt_clip"):
            path = f"/bot_files/null/yt_cache/{db_obj['filename']}"
        elif("name" in db_obj):
            path = f"/bot_files/{guild_id}/sounds/{db_obj['filename']}"
        mime = mimetypes.guess_type(path)[0]
        with open(path, "rb") as f:
            return Response(f.read(), mimetype=mime)
    else:
        abort(404)


@app.route("/api/v1/login")
def back_login():
    code = request.args.get("code")
    state = request.args.get("state")
    if not code or not state or not state == session.get("TOKEN"):
        return redirect(url_for("frnt_login"))
    resp = exchange_code(code)

    user_guilds = get_user_guilds(resp["access_token"])
    ret = {}
    for g in user_guilds:
        response = guilds.find_one(
            {"_guild_id": str(g["id"])},
            {"name": 1, "icon": 1})
        if response:
            ret[response["_id"]] = response

    sessions[session["TOKEN"]] = {
        "access_token": resp["access_token"],
        "refresh_token": resp["refresh_token"],
        "token_expires": time.time() - resp["expires_in"],
        "logged_in": True,
        "guilds": ret
    }
    return render_template("/closePopup.html")


@app.route("/login")
def frnt_login():
    try:
        if(sessions[session["TOKEN"]]["logged_in"]):
            return redirect(url_for("guilds_view"))
    except KeyError:
        pass
    return render_template("/login.html")


@app.route("/api/v1/token")
def back_token():
    """Returns the users session token"""
    if not session.get("TOKEN"):
        session["TOKEN"] = secrets.token_urlsafe(16)
    return {"session_token": session["TOKEN"]}


def exchange_code(code):
    data = {
            "client_id": DISCORD_CLIENT_ID,
            "client_secret": DISCORD_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": REDIRECT_URI
        }
    r = requests.post(
        f"{API_ENDPOINT}/oauth2/token",
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data=data
        )
    r.raise_for_status()
    return r.json()


if __name__ == "__main__":
    app.run(host="0.0.0.0")

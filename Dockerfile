FROM python:buster
RUN mkdir /app
RUN apt update
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt
COPY app.py /app/app.py
COPY wsgi.py /app/wsgi.py
WORKDIR /app/
ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:5000", "wsgi:app"]